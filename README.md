# Febrafar

## Sobre

A aplicação foi desenvolvida com o objetivo simples de cadastrar colaboratores e manter o histórico de salários.
Foi desenvolvida em ambiente [*Linux - CentOS 8*](https://www.centos.org/), sobre o servidor web [*Apache 2*](https://www.apache.org/) e tecnologia [*PHP 8.0.14*](https://www.php.net/manual/pt_BR/intro-whatis.php) e [*Laravel 8.75*](https://laravel.com/).

## Instalação

Para realizar a instalaçao, e necessario conhecimento tecnico basico de:

* PHP;
* Laravel;
* Git;
* Banco de Dados Relacional;
* Web - Cliente Servidor.

**Clonar Repositorio**

```bash
$ git clone https://kevipezenti@bitbucket.org/kevipezenti/febrafar.git
```

**Dependências**

Entre no diretorio `febrafar` criado na execuçao do comando acima, e utilize o gerenciador de pacotes [composer](https://getcomposer.org/) para instalar as dependências da aplicação.

```bash
$ composer install
```

**Configuraçao de Ambiente**

Configure o seu arquivo de ambiente conforme orientaçao da [documentaçao](https://laravel.com/docs/8.x/configuration#introduction) do Laravel.

Obs.: Alem das variaveis indispensaveis para o funcionamento do Laravel, e indispensavel a configuraçao de um SGDB.

**Banco de dados**

Apos a configuraçao do ambiente, e certificar que as variaveis de acesso ao banco de dados estao corretas, rode as migraçoes com o comando:

```bash
php artisan migrate
```

## Primeiro Acesso

Para realizar o primeiro acesso verifique se o servidor web, esteja instalado e  executando.

## Rotas Web

Apos verificar que todos os itens acima citados estao corretos. Acesse a URL e porta, caso definida, na variavel `APP_URL`, no arquivo de configuraçao de ambiente `.env`.

**CRUD Colaboradores**

**GET** - `/api/collaborators`

Retorna todos os colaboradores cadastrados.

*Response:*

```json
[
  {
    "id": 2,
    "name": "Teste",
    "email": "teste2@gmail.com",
    "cpf": "2925564038",
    "rg": "2345 stp",
    "birth_date": "2021-12-12",
    "zip_code": "29090610",
    "address": "Rua teste",
    "number": "205",
    "city": "Vitoria",
    "state": "es",
    "created_at": "2021-12-31T10:35:47.000000Z",
    "updated_at": "2021-12-31T10:35:47.000000Z"
  },
  {
    "id": 3,
    "name": "Teste",
    "email": "teste@gmail.com",
    "cpf": "31124782079",
    "rg": "234 stpc",
    "birth_date": "2021-12-12",
    "zip_code": "29090610",
    "address": "Rua teste",
    "number": "205",
    "city": "Vitoria",
    "state": "es",
    "created_at": "2021-12-31T10:40:52.000000Z",
    "updated_at": "2021-12-31T10:40:52.000000Z"
  }
]
```

---

**GET** - `/api/collaborators/(request)`

Retorna o colaborador por ID ou por CPF, com o historico de salario.

*Response:*

```json
[
  {
    "id": 3,
    "name": "Teste",
    "email": "teste@gmail.com",
    "cpf": "31124782079",
    "rg": "234 stpc",
    "birth_date": "2021-12-12",
    "zip_code": "29090610",
    "address": "Rua teste",
    "number": "205",
    "city": "Vitoria",
    "state": "es",
    "created_at": "2021-12-31T10:40:52.000000Z",
    "updated_at": "2021-12-31T10:40:52.000000Z"
    "wages": [
      {
        "id": 1,
        "collaborator_id": "3",
        "value": "5000",
        "created_at": "2021-12-31T13:26:57.000000Z",
        "updated_at": "2021-12-31T13:26:57.000000Z"
      },
      {
        "id": 2,
        "collaborator_id": "3",
        "value": "15000",
        "created_at": "2021-12-31T13:31:00.000000Z",
        "updated_at": "2021-12-31T13:31:00.000000Z"
      }
    ]
  }
]
```

---

**POST** - `/api/collaborators`

Adiciona um colaborador e retorna os dados cadastrados com ID.

> Obs.: Todos os parametros sao obrigatorios.

*Request:*

```json
{
    "name": "Teste",
    "email": "teste@gmail.com",
    "cpf": "311.247.820-79",
    "rg": "234 stpc",
    "birth_date": "2021-12-12",
    "zip_code": "29090-610",
    "address": "Rua teste",
    "number": "205",
    "city": "Vitoria",
    "state": "es",
}
```

*Response:*

```json
{
    "name": "Teste",
    "email": "teste@gmail.com",
    "cpf": "311.247.820-79",
    "rg": "234 stpc",
    "birth_date": "2021-12-12", 
    "zip_code": "29090-610",
    "address": "Rua teste",
    "number": "205",
    "city": "Vitoria",
    "state": "es",
    "updated_at": "2021-12-31T10:40:52.000000Z",
    "created_at": "2021-12-31T10:40:52.000000Z",
    "id": 3
}
```

---

**PUT** - `/api/collaborators/(id)`

Atualizar um colaborador por id.

> Obs.: Todos os parametros sao obrigatorios.

*Request:*

```json
{
    "name": "Teste",
    "email": "teste3@gmail.com",
    "cpf": "311.247.820-79",
    "rg": "234 stpc",
    "birth_date": "2021-12-12",
    "zip_code": "29090-610",
    "address": "Rua teste2",
    "number": "205",
    "city": "Cidade teste",
    "state": "SP",
}
```

---

**DELETE** - `/api/collaborators/(id)`

Remove o colaborador por id.

---

**Salario**

**POST** - `/api/wages`

Adiciona o salario ao colaborador informado por id e retorna dos dados cadastrado com id de cadastro.

*Request:*

```json
{
    "collaborator_id": 3,
    "value": 15000.00
}
```

*Response:*

```json
{
  "collaborator_id": 3,
  "value": 15000,
  "updated_at": "2021-12-31T13:31:00.000000Z",
  "created_at": "2021-12-31T13:31:00.000000Z",
  "id": 2
}
```

## Contribuições

Solicitações pull são bem-vindas. Para mudanças importantes, abra um problema primeiro para discutir o que você gostaria de mudar.

## License

[MIT](https://choosealicense.com/licenses/mit/)
