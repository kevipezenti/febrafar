<?php

namespace App\Exceptions\Request;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class ApiRequestExceptions extends HttpResponseException
{
    public function __construct(protected Validator $validator)
    {
        parent::__construct($this->makeResponse());
    }

    /**
     * @return Response
     */
    protected function makeResponse(): Response
    {
        return response()
            ->json(
                $this->validator->errors(),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
    }
}
