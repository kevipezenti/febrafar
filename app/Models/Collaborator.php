<?php

namespace App\Models;

use App\Traits\CollaboratorRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Collaborator extends Model
{
    use CollaboratorRepository;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'cpf',
        'rg',
        'birth_date',
        'zip_code',
        'address',
        'number',
        'city',
        'state',
    ];

    /**
     * @return HasMany|Wage
     */
    public function wages(): HasMany|Wage
    {
        return $this->hasMany(Wage::class);
    }
}
