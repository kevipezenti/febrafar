<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Wage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'collaborator_id',
        'value'
    ];

    /**
     * @return BelongsTo|Collaborator
     */
    public function collaborators(): BelongsTo|Collaborator
    {
        return $this->belongsTo(Collaborator::class);
    }
}
