<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;

trait CollaboratorRepository
{
    /**
     * @param int $request
     * @return Collection
     */
    public function idOrCPF(int $request): Collection
    {
        return $this->with(['wages'])
            ->whereId($request)
            ->orWhere('cpf', $request)
            ->get();
    }
}
