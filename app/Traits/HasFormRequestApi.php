<?php

namespace App\Traits;

use App\Exceptions\Request\ApiRequestExceptions;
use Illuminate\Contracts\Validation\Validator;

trait HasFormRequestApi
{
    /**
     * @param Validator $validator
     * @return void
     */
    public function failedValidation(Validator $validator): void
    {
        throw new ApiRequestExceptions($validator);
    }
}
