<?php

namespace App\Http\Requests;

use App\Rules\CPFValidation;
use App\Traits\HasFormRequestApi;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CollaboratorRequest extends FormRequest
{
    use HasFormRequestApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3'],
            'email' => [
                'required',
                'email',
                'max:254',
                Rule::unique('collaborators')
                    ->ignore(
                        optional($this->collaborator)->id
                    )
            ],
            'cpf' => [
                'required',
                'string',
                'size:11',
                new CPFValidation,
                Rule::unique('collaborators')
                    ->ignore(
                        optional($this->collaborator)->id
                    )
            ],
            'rg' => ['required', 'string', 'min:4', 'max:15'],
            'birth_date' => ['required', 'date', 'date_format:Y-m-d'],
            'zip_code' => ['required', 'string', 'size:8'],
            'address' => ['required', 'string'],
            'number' => ['required', 'string', 'min:1', 'max:5'],
            'city' => ['required', 'string'],
            'state' => ['required', 'string'],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'cpf' => preg_replace('/\D/', '', $this->cpf),
            'zip_code' => preg_replace('/\D/', '', $this->zip_code),
        ]);
    }
}
