<?php

namespace App\Http\Controllers;

use App\Http\Requests\CollaboratorRequest;
use App\Models\Collaborator;
use Illuminate\Http\Response;

class CollaboratorController extends Controller
{
    public function __construct(protected Collaborator $collaborator)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        return response($this->collaborator->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CollaboratorRequest $request
     * @return Response
     */
    public function store(CollaboratorRequest $request): Response
    {
        return response($this->collaborator->create($request->validated()));
    }


    /**
     * Display the specified resource.
     *
     * @param int $request
     * @return Response
     */
    public function show(int $request): Response
    {
        return response($this->collaborator->idOrCPF($request));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CollaboratorRequest $request
     * @param Collaborator $collaborator
     * @return Response
     */
    public function update(CollaboratorRequest $request, Collaborator $collaborator): Response
    {
        return response($collaborator->update($request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Collaborator $collaborator
     * @return Response
     */
    public function destroy(Collaborator $collaborator): Response
    {
        return response($collaborator->delete());
    }
}
