<?php

namespace App\Http\Controllers;

use App\Http\Requests\WageRequest;
use Illuminate\Http\Response;
use App\Models\Wage;

class WageController extends Controller
{
    public function __construct(protected Wage $wage)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param WageRequest $request
     * @return Response
     */
    public function store(WageRequest $request): Response
    {
        info('Request ', $request->validated());
        return response($this->wage->create($request->validated()));
    }

}
