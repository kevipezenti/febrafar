<?php

use App\Http\Controllers\CollaboratorController;
use App\Http\Controllers\WageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('collaborators', CollaboratorController::class);

Route::post('wages', [WageController::class , 'store']);
